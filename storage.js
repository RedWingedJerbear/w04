window.onload = function () {
  // We can use getters and setters
  // or properties to access sessionStorage (or localStorage)
  if(localStorage.getItem("firstName") === null){
    const fname = prompt("What is your first name", "Emmett");;
    if (fname != null) {
      localStorage.setItem("firstName", fname);
    }
  }

  const s = localStorage.getItem("firstName") + ", what is your last name"
  if(localStorage.getItem("lastName") === null){
    const lname = prompt(s, "Awesome"); 
    if (lname != null) {
      localStorage.setItem("lastName",lname);
    }
  }
  
  alert("Hello " + localStorage.firstName + " " + localStorage.lastName)

  // We cannot test the following in Chrome.
  // Chrome blocks cookies running on localhost
  // for security reasons.

  document.cookie = "firstName=Emmett"
  document.cookie = "lastName=Aweseome"
}