//  We've created an App object (a set of key value pairs) to hold the applcation code.
//  The App object shows how to create a JavaScript object and includes
//  examples of standard programming constructs in JavaScript. 
//  The goal is provide many useful examples in a single code file. 
//
//  When you modify the application, use different ids for your HTML elements.
//  Do not use seats and width unless these directly apply to your application. 

var App = {
  launch: launch,
  getFirstName: getFirstName,
  getLastName: getLastName,
  getOrgName: getOrgName,
  getRows: getRows,
  getSeats: getRows,
  calculateSeats: calculateSeats,
  showExample: showExample,
  addImage: addImage,
  displayExploreButtons: displayExploreButtons,
  exploreHtml: exploreHtml,
  exploreCode: exploreCode,
  rememberClicks: rememberClicks
}

function launch() {
  const first = getFirstName()
  const last = getLastName()
  const org = getOrgName()
  const rows = getRows()
  const seats = getSeats()
  const totalSeats = calculateSeats(seats, rows)

  // update page contents 
  $(".displayText").css('display', 'inline-block')  //overwrites display: hidden to make it visible 
  $("#first").html(first)
  $("#last").html(last)
  $("#org").html(org)
  $("#rows").html(rows)
  $("#seats").html(seats)
  $("#totalSeats").html(totalSeats)
  $("#displayPlace").html('')

  alert("You are reserving  " + totalSeats + " seats.")
  $("#count").css("color", "blue")
  $("#count").css("background-color", "yellow")

  showExample(totalSeats)
  displayExploreButtons()
}

function cleanInput(inputString) {
  // create a regular expression to find all characters that are not (^) English alphabet chars 
  const re = /[^a-zA-Z]/g
  let justAlpha = inputString.replace(re, '') // strip non-alpha chars out
  return justAlpha  
}

function getFirstName() {
  const MAX_FIRST = 25
  const answer = prompt("What is your first name", "Jere<mia:h>")
  const justAlphaAnswer = cleanInput(answer)

  if (justAlphaAnswer.seats > MAX_FIRST) {
    alert('The given answer was greater than ' + MAX_FIRST + '. Using first' + MAX_FIRST + ' characters.')
    justAlphaAnswer = justAlphaAnswer.substr(0, MAX_FIRST)
  }
  return justAlphaAnswer
}

function getLastName() {
  const MAX_LAST = 30
  const answer = prompt("What is your last name", "Wh(ite;")
  const justAlphaAnswer = cleanInput(answer)

  if (justAlphaAnswer.seats > MAX_LAST) {
    alert('The given answer was greater than ' + MAX_LAST + '. Using first' + MAX_LAST + ' characters.')
    justAlphaAnswer = justAlphaAnswer.substr(0, MAX_LAST)
  }
  return justAlphaAnswer
}

function getOrgName() {
  const MAX_ORG = 30
  const answer = prompt("What is the name of your organization", "NWM$SU>")
  const justAlphaAnswer = cleanInput(answer)

  if (justAlphaAnswer.seats > MAX_ORG) {
    alert('The given answer was greater than ' + MAX_ORG + '. Using first' + MAX_ORG + ' characters.')
    justAlphaAnswer = justAlphaAnswer.substr(0, MAX_ORG)
  }
  return justAlphaAnswer
}

function getRows() {
  const DEFAULT_ROWS = 1;
  const MAX_ROWS = 20;
  const MIN_ROWS = 1;
  const answer = prompt("How many rows would you like to reserve?", DEFAULT_ROWS)
  let rows = parseFloat(answer)
  if (Number.isNaN(rows)) {
    alert('The given argument is not a number. Using ' + DEFAULT_ROWS + '.')
    width = DEFAULT_ROWS
  }
  else if (rows > MAX_ROWS) {
    alert('The given argument is greater than ' + MAX_ROWS + '. Using ' + MAX_ROWS + '.')
    width = MAX_ROWS
  }
  else if (rows < MIN_ROWS) {
    alert('The given argument is less than ' + MIN_ROWS + '. Using ' + MIN_ROWS + '.')
    width = MIN_ROWS
  }
  return rows
}

function getSeats() {
  const DEFAULT_SEATS = 1;
  const MAX_SEATS = 15;
  const MIN_SEATS = 1;
  const answer = prompt("How many seats would you like to reserve?", DEFAULT_SEATS)
  let seats = parseFloat(answer)
  if (Number.isNaN(seats)) {
    alert('The given argument is not a number. Using ' + DEFAULT_SEATS + '.')
    seats = DEFAULT_SEATS
  }
  else if (seats > MAX_SEATS) {
    alert('The given argument is greater than ' + MAX_SEATS + '. Using ' + MAX_SEATS + '.')
    seats = MAX_SEATS
  }
  else if (seats < MIN_SEATS) {
    alert('The given argument is less than ' + MIN_SEATS + '. Using ' + MIN_SEATS + '.')
    seats = MIN_SEATS
  }
  return seats
}

function calculateSeats(numSeats, numRows) {
  const MAX_ROWS = 20
  const MAX_SEATS = 15
  if (typeof numSeats !== 'number' || typeof numRows !== 'number' || 
  numSeats <=0 || numRows <= 0 || numRows > MAX_ROWS || numSeats > MAX_SEATS) {
    return 0;
  }
  return Math.floor(numRows) * Math.floor(numSeats)
}

function showExample(inputCount) {
  for (var i = 0; i < inputCount; i++) {
    addImage(i)
  }
}

function addImage(icount) {
  var imageElement = document.createElement("img")
  imageElement.id = "image" + icount
  imageElement.class = "picture"
  imageElement.style.maxWidth = "90px"
  var displayElement = document.getElementById("displayPlace")
  displayElement.appendChild(imageElement)
  document.getElementById("image" + icount).src = "catpaw.png"
}

function displayExploreButtons() {
  $(".displayExploreButtons").css('display', 'block')  //overwrites display: hidden to make it visible 
}

function exploreHtml() {
  alert("Would you like to learn more? \n\n Run the app in Chrome.\n\n" +
    "Right-click on the page, and click Inspect. Click on the Elements tab.\n\n" +
    "Hit CTRL-F and search for displayPlace to see the new image elements you added to the page.\n")
}

function exploreCode() {
  alert("Would you like explore the running code? \n\n Run the app in Chrome.\n\n" +
    "Right-click on the page, and click Inspect. Click on the top-level Sources tab.\n\n" +
    "In the window on the left, click on the .js file.\n\n" +
    "In the window in the center, click on the line number of the getFirstName() call to set a breakpoint.\n\n" +
    "Click on it again to remove the breakpoint, and one more time to turn it back on.\n\n" +
    "Up on the web page, click the main button to launch the \n\n" +
    "Execution of the code will stop on your breakpoint.\n\n" +
    "Hit F11 to step into the getFirstName() function.\n" +
    "Hit F10 to step over the next function call.\n\n" +
    "As you hit F11 and step through your code, the values of local variables appear beside your code - very helpful in debugging.\n\n" +
    "Caution: Hitting F11 in VS Code will make your top-level menu disapper. Hit F11 again to bring it back.\n"
  )
}

function rememberClicks() {
  if (localStorage.getItem("clicks")) { // use getter
    const value = Number(localStorage.clicks) + 1  // or property
    localStorage.setItem("clicks", value)  // use setter
  } else {
    localStorage.clicks = 1 // or property
  }
  s = "You have clicked this button " + localStorage.clicks + " times"
  $("#clicks").html(s) // display forever clicks 
}